#!/bin/bash
DATA_DIR=/opt
DL_DIR=/media/unsorted
MEDIA_DIR=/media/videos
SAB_PORT="8081:8080"
SONARR_PORT="8082:8989"
COUCH_PORT="8083:5050"

usage(){
	printf "$(basename $0) [install|clean]


This script will install sabnzbd, sonarr and couchpotato using docker. Docker will be installed if it is not currenting installed.
The script should be run as root (or using sudo).


install 	will install systemd unit files, the start and enable the service to start on boot
clean		will remove the sonarr, sabnzbd and couchpotato docker containers as well as the ${DATA_DIR}/sonarr, ${DATA_DIR}/sabnzbd and ${DATA_DIR}/couchpotato
*		this help text

"
	exit 1
}

cleanup(){
	while true
	do
		printf "This will remove the sonarr, sabnzbd and couchpotato docker containers as well as the ${DATA_DIR}/sonarr, ${DATA_DIR}/sabnzbd and ${DATA_DIR}/couchpotato.

Are you sure you want to do this? [y/N]\n"
		read confirm
		case ${confirm:=N} in
			y|Y)
				echo "Removing docker containers..."
				docker rm -f sonarr sabnzbd couchpotato
				echo "Removing directories..."
				rm -rf ${DATA_DIR}/{sonarr,sabnzbd,couchpotato}
				break
				;;
			n|N)
				break
				;;
			*)
				echo "Please enter 'y' or 'n'"
	esac	
	done
			
}

handle_signal() {
  echo "Exiting..."
  exit 1
}

trap "handle_signal" SIGINT SIGTERM SIGHUP


prep(){
	local data_dir=$1
	echo "Creating directories and updating selinux policy..."
	mkdir -p $data_dir
	chcon -Rt svirt_sandbox_file_t $data_dir
	echo "Done"
}

# Make sure docker is instaled
dockercheck(){
	local installed=true
	docker --version > /dev/null 2>&1 || installed=false
	if $installed
	then
		print "Docker installed\n\n"
	else
		printf "Error: Docker is not installed. Please installed docker before continuing\n\n"
		exit 1
	fi
}


unitfile(){
	local name=$(echo "$1" | tr [:upper:] [:lower:])
	# each docker container has slightly different volume mounts
	case "$name" in
		sonarr)
			local port="-p $2"
			local data_dir=""
			local dl_dir=""
			local media_dir=""
			;;
		sabnzbd)
			local port="-p $2"
			local data_dir="-v ${3}:/datadir"
			local dl_dir=""
			local media_dir="-v ${5}:/media"
			;;
		couchpotato)
			local port="-p $2"
			local data_dir="-v ${3}:/datadir"
			local dl_dir=" -v ${4}:/download"
			local media_dir="-v ${5}:/media"
			;;
		*)
			echo "ERROR: service not supported"
			exit 1
	esac
		
	local docker_image=$6
	
	echo "Creating systemd unit file for $name service..."
	printf "[Unit]
Description=${name}-docker
After=docker.service
Wants=docker.service
[Service]
TimeoutStartSec=0
ExecStartPre=-/usr/bin/docker rm -f ${name}
ExecStartPre=/usr/bin/docker pull ${docker_image}
ExecStart=/bin/docker run --name ${name} ${port} ${data_dir} ${dl_dir} ${media_dir} --rm ${docker_image}
ExecStop=/bin/docker stop ${name}
[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/${name}.service

	echo "Done"
	systemdreload $name
}

systemdreload(){
	local name=$1
	echo "Reloading systemd, restarting and enabling $name to run on boot"
	systemctl daemon-reload
	systemctl restart $name
	systemctl enable $name
	echo "Done"
}

# Check if run by root
main(){


if [[ $EUID -ne 0 ]]
then
	echo "ERROR: Need to run as root user"
	usage
fi

case $1 in
	clean)
		cleanup
		exit 0
		;;
	install)
		INSTALL=true
		;;
	*)
		usage
esac	

dockercheck
echo "Checking that IPv4 forwarding is enabled..."
sysctl -w net.ipv4.ip_forward=1
echo "net.ipv4.ip_forward=1" | tee /etc/sysctl.d/ipv4_forward

prep "${DATA_DIR}/sabnzbd"
unitfile "sabnzbd" "$SAB_PORT" "${DATA_DIR}/sabnzbd" "${DL_DIR}" "" "nrwiersma/docker-sabnzbd"

prep "${DATA_DIR}/sonarr"
unitfile "sonarr" "$SONARR_PORT" ""  "" "" "ryguy/docker-sonarr"

prep "${DATA_DIR}/couchpotato"
unitfile "couchpotato" "$COUCH_PORT" "${DATA_DIR}/couchpotato" "${DL_DIR}" "${MEDIA_DIR}" "nrwiersma/docker-couchpotato"
}

main $1
